## METADATAMANAGER SPEC API v0.1

This document describes how to setup, start and stop a dataset on a MetadataManager Tango Device by using SPEC.

### METHODS

| Name         | Usage                                      | Description                                                                                                     |
| --------     | -------------------------------------------| --------------------------------------------------------------------------------------------------------------- |
| mdatainfo    |                                            |  prints information of the state of MedataManager parameters (Proposal, sample, state, etc..                    |
| mdatareset   |                                            |  sets the state of MetadataManager to off and remove information about proposal, sample and dataset             |
| newproposal  | newproposal <proposal> <dataRoot>          |  sets the proposal and dataRoot (output path where files will be generated)                                     |
| newsample    | newsample <new sample>                     |  sets the sample | 
| datasetstart | datasetstart <dataset name>                |  sets the state to RUNNING and start to "record" data |
| datasetend   | 			                    |  stops to record and sends the information to the database. |


| Name         | Usage                                      | Description                                                                                                     |
| --------     | -------------------------------------------| --------------------------------------------------------------------------------------------------------------- |
| mdatasetup   | <metadataExperiment dev> <metadataManager dev>|setup of the metadata devices|
| mdataon      |                                               |enable the metadata process|
| mdataoff     |                                               | disable the metadata process|
| mdatareset   |                                               |reset the metadata devices (proposal, sample, dataset)|
| mdatanewproposal |<proposal> <dataRoot>|          open a proposal with the dataRoot|
| mdataendproposal|                                            | close the proposal|
| mdatanewsample| <sample>|definition of the sample, description and technique will be requested|
| mdatasetstart |<datasetName>|start the dataset|
| mdatasetend||end the running dataset|
| mdatasetinterrupt||interrupt the running dataset|
| mdatasetabort|| abort the running dataset|
| mdatagetfolder ||get the working dataset directory|
| mdatasetfolder |(subdirectory)| create a sudirectory in the working dataset directory|
| mdatadebug |[<debug level>]| set/get the debug level|
| mdatamono |[<parameter> <value] [<parameter> <value] ...| set the monochromator parameters for metadata|

#### mdatainfo

Prints the information about the Meadata Manager

Usage:
```
358.RHMETA> mdatainfo 
   mdataon: [ON]
     state: [NOSAMPLE]
  proposal: exp[mx0004] mgr[mx0004]
    sample: exp[please enter] mgr[]
   dataset: [invalid]
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/{sampleName}/{scanName}]

```

#### mdatareset

Set information of sample and dataset to invalid and set the state to OFF of the metadata manager

Usage:
```
358.RHMETA> mdatainfo 
   mdataon: [ON]
     state: [NOSAMPLE]
  proposal: exp[mx0004] mgr[mx0004]
    sample: exp[please enter] mgr[]
   dataset: [invalid]
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/{sampleName}/{scanName}]

opid21's state is stored

359.RHMETA> mdatareset
... INFO    [0]
   mdataon: [ON]
     state: [OFF]
  proposal: []
    sample: [invalid]
   dataset: [invalid]
dataFolder: [/data/visitor/{proposal}/id21/{sampleName}/{scanName}]

```


#### newproposal

Defines the proposal and the data root in the Metadata Manager. It sets state ON and gets it ready for the sample

Usage:
```
newproposal <proposal> <dataRoot>
```


Example:
```
357.RHMETA> newproposal mx0004 /data/id21/inhouse/metadata/
... INFO    [newproposal]
   mdataon: [ON]
     state: [NOSAMPLE]
  proposal: [mx0004]
    sample: []
   dataset: [invalid]
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/{sampleName}/{scanName}]

```

#### newsample

Defines the sample and also asks interactively to users about the description and the technique

Usage:
```
newsample <new sample>
```
Example:
```
362.RHMETA> newsample A3
sample description ()? This is the description of my sample called A3
technique ()? XANES
description [This is the description of my sample called A3]
  technique [XANES]
... INFO    [newsample]
   mdataon: [ON]
     state: [STANDBY]
  proposal: [mx0004]
    sample: [A3]
   dataset: []
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/A3/{scanName}]
```

#### datasetstart

Itsets the state to RUNNING and start to "record" data. If no data set name is given then it will be automatically set with the name of the sample and a autoincremental identifier.

Usage:
```
datasetstart
```
```
datasetstart datasetName
```
Example:
```
363.RHMETA> datasetstart 
... INFO    [datasetstart]
   mdataon: [ON]
     state: [RUNNING]
  proposal: [mx0004]
    sample: [A3]
   dataset: [A3_0317]
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/A3/A3_0317]

```

However, a more convient way is to set a name of the dataset
```
365.RHMETA> datasetstart ALINEATION
... INFO    [datasetstart]
   mdataon: [ON]
     state: [RUNNING]
  proposal: [mx0004]
    sample: [A3]
   dataset: [A3_ALINEATION]
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/A3/A3_ALINEATION]
```


#### datasetend

Defines the sample and also asks interactively to users about the description and the technique

Usage:
```
datasetend
```
Example:
```
366.RHMETA> datasetend
... INFO    [datasetend]
   mdataon: [ON]
     state: [STANDBY]
  proposal: [mx0004]
    sample: [A3]
   dataset: []
dataFolder: [/data/id21/inhouse/metadata/mx0004/id21/A3/{scanName}]

```