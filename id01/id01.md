## Config

### SPEC MACRO
```
ssh opid01@nano2
rhmeta_test
```

reconfig (but better to restart the session control+d and log in)

### DEVICE SERVER
```
ssh -X opid01@nano2
bliss_dserver start MetadataManager id01test
bliss_dserver start MetaExperiment id01test

```
Check status:
```
nano2:~ % bliss_dserver status
TangoSpec :                 optics opticsmeta psic_nano slits_p1 slits_p2 slits_s1 slits_s2 slits_s3 slits_s4 slits_s5 slits_s6 blserver
MetaExperiment :            id01test
MetadataManager :           id01test
ElettraElectrometerDS :     None
```

### Config DEVICES

/users/blissadm/local/daemon/config
\* means that will NOT start automatically

```
[MetaExperiment]
*id01
*id01test

[MetadataManager]
*id01
*id01test
```


### Developing 

```
cd /users/blissadm/tango-metadata/MetadataManager
python MetadataManager.py id01test -v4

```


### Parameters

```
technique_name
InstrumentSource_mode = orion:10000/fe/id/1/SR_Filling_Mode
InstrumentSource_current = orion:10000/fe/id/1/SR_Current
InstrumentInsertionDevice_gap_value = orion:10000/id/id/1/U35a_GAP_Position,orion:10000/id/id/1/U27b_GAP_Position, orion:10000/id/id/1/U35b_GAP_Position, orion:10000/id/id/1/U27c_GAP_Position
InstrumentInsertionDevice_taper_value =orion:10000/id/id/1/U35a_TAPER_Position, orion:10000/id/id/1/U27c_TAPER_Position
Sample_description
InstrumentMonochromator_wavelength
InstrumentMonochromatorCrystal_usage
InstrumentMonochromatorCrystal_d_spacing
InstrumentMonochromatorCrystal_type
InstrumentMonochromatorCrystal_reflection
InstrumentSlitPrimary_vertical_gap = nano1:20000/ID01/spec_nano2_slits/p1vg/Position
InstrumentSlitPrimary_vertical_offset = nano1:20000/ID01/spec_nano2_slits/p1vo/Position
InstrumentSlitPrimary_horizontal_gap = nano1:20000/ID01/spec_nano2_slits/p1hg/Position
InstrumentSlitPrimary_horizontal_offset = nano1:20000/ID01/spec_nano2_slits/p1ho/Position
InstrumentSlitPrimary_blade_up = nano1:20000/ID01/spec_nano2_slits/p1u/Position
InstrumentSlitPrimary_blade_down = nano1:20000/ID01/spec_nano2_slits/p1d/Position
InstrumentSlitPrimary_blade_front = nano1:20000/ID01/spec_nano2_slits/p1f/Position
InstrumentSlitPrimary_blade_back = nano1:20000/ID01/spec_nano2_slits/p1b/Position
InstrumentSlitSecondary_vertical_gap = nano1:20000/ID01/spec_nano2_slits/s1vg/Position
InstrumentSlitSecondary_vertical_offset = nano1:20000/ID01/spec_nano2_slits/s1vo/Position
InstrumentSlitSecondary_horizontal_gap = nano1:20000/ID01/spec_nano2_slits/s1hg/Position
InstrumentSlitSecondary_horizontal_offset = nano1:20000/ID01/spec_nano2_slits/s1ho/Position
InstrumentSlitSecondary_blade_up = nano1:20000/ID01/spec_nano2_slits/s1u/Position
InstrumentSlitSecondary_blade_down = nano1:20000/ID01/spec_nano2_slits/s1d/Position
InstrumentSlitSecondary_blade_front = nano1:20000/ID01/spec_nano2_slits/s1f/Position
InstrumentSlitSecondary_blade_back = nano1:20000/ID01/spec_nano2_slits/s1b/Position
InstrumentSlits_vertical_gap = nano1:20000/ID01/spec_nano2_slits/p1vg/Position,nano1:20000/ID01/spec_nano2_slits/p2vg/Position,nano1:20000/ID01/spec_nano2_slits/s1vg/Position,nano1:20000/ID01/spec_nano2_slits/s2vg/Position,nano1:20000/ID01/spec_nano2_slits/s3vg/Position,nano1:20000/ID01/spec_nano2_slits/s4vg/Position,nano1:20000/ID01/spec_nano2_slits/s5vg/Position,nano1:20000/ID01/spec_nano2_slits/s6ag/Position
InstrumentSlits_vertical_offset = nano1:20000/ID01/spec_nano2_slits/p1vo/Position,nano1:20000/ID01/spec_nano2_slits/p2vo/Position,nano1:20000/ID01/spec_nano2_slits/s1vo/Position,nano1:20000/ID01/spec_nano2_slits/s2vo/Position,nano1:20000/ID01/spec_nano2_slits/s3vo/Position,nano1:20000/ID01/spec_nano2_slits/s4vo/Position,nano1:20000/ID01/spec_nano2_slits/s5vo/Position,nano1:20000/ID01/spec_nano2_slits/s6ao/Position
InstrumentSlits_horizontal_gap = nano1:20000/ID01/spec_nano2_slits/p1hg/Position,nano1:20000/ID01/spec_nano2_slits/p2hg/Position,nano1:20000/ID01/spec_nano2_slits/s1hg/Position,nano1:20000/ID01/spec_nano2_slits/s2hg/Position,nano1:20000/ID01/spec_nano2_slits/s3hg/Position,nano1:20000/ID01/spec_nano2_slits/s4hg/Position,nano1:20000/ID01/spec_nano2_slits/s5hg/Position,nano1:20000/ID01/spec_nano2_slits/s6bg/Position
InstrumentSlits_horizontal_offset = nano1:20000/ID01/spec_nano2_slits/p1ho/Position,nano1:20000/ID01/spec_nano2_slits/p2ho/Position,nano1:20000/ID01/spec_nano2_slits/s1ho/Position,nano1:20000/ID01/spec_nano2_slits/s2ho/Position,nano1:20000/ID01/spec_nano2_slits/s3ho/Position,nano1:20000/ID01/spec_nano2_slits/s4ho/Position,nano1:20000/ID01/spec_nano2_slits/s5ho/Position,nano1:20000/ID01/spec_nano2_slits/s6bo/Position
InstrumentSlits_blade_up = nano1:20000/ID01/spec_nano2_slits/p1u/Position,nano1:20000/ID01/spec_nano2_slits/p2u/Position,nano1:20000/ID01/spec_nano2_slits/s1u/Position,nano1:20000/ID01/spec_nano2_slits/s2u/Position,nano1:20000/ID01/spec_nano2_slits/s3u/Position,nano1:20000/ID01/spec_nano2_slits/s4u/Position,nano1:20000/ID01/spec_nano2_slits/s5u/Position,nano1:20000/ID01/spec_nano2_slits/s6ag/Position
InstrumentSlits_blade_down = nano1:20000/ID01/spec_nano2_slits/p1d/Position,nano1:20000/ID01/spec_nano2_slits/p2d/Position,nano1:20000/ID01/spec_nano2_slits/s1d/Position,nano1:20000/ID01/spec_nano2_slits/s2d/Position,nano1:20000/ID01/spec_nano2_slits/s3d/Position,nano1:20000/ID01/spec_nano2_slits/s4d/Position,nano1:20000/ID01/spec_nano2_slits/s5d/Position,nano1:20000/ID01/spec_nano2_slits/s6ao/Position
InstrumentSlits_blade_front = nano1:20000/ID01/spec_nano2_slits/p1f/Position,nano1:20000/ID01/spec_nano2_slits/p2f/Position,nano1:20000/ID01/spec_nano2_slits/s1f/Position,nano1:20000/ID01/spec_nano2_slits/s2f/Position,nano1:20000/ID01/spec_nano2_slits/s3f/Position,nano1:20000/ID01/spec_nano2_slits/s4f/Position,nano1:20000/ID01/spec_nano2_slits/s5f/Position,nano1:20000/ID01/spec_nano2_slits/s6bg/Position
InstrumentSlits_blade_back = nano1:20000/ID01/spec_nano2_slits/p1b/Position,nano1:20000/ID01/spec_nano2_slits/p2b/Position,nano1:20000/ID01/spec_nano2_slits/s1b/Position,nano1:20000/ID01/spec_nano2_slits/s2b/Position,nano1:20000/ID01/spec_nano2_slits/s3b/Position,nano1:20000/ID01/spec_nano2_slits/s4b/Position,nano1:20000/ID01/spec_nano2_slits/s5b/Position,nano1:20000/ID01/spec_nano2_slits/s6bo/Position
InstrumentOpticsMotors_value = nano1:20000/ID01/spec_nano2_optics/monobof/Position, nano1:20000/ID01/spec_nano2_optics/mononrj/Position, nano1:20000/ID01/spec_nano2_optics/monoth/Position, nano1:20000/ID01/spec_nano2_optics/mir1rz/Position, nano1:20000/ID01/spec_nano2_optics/mir1rzf/Position, nano1:20000/ID01/spec_nano2_optics/mir2crv/Position, nano1:20000/ID01/spec_nano2_optics/mir2ty/Position, nano1:20000/ID01/spec_nano2_optics/mirboff/Position, nano1:20000/ID01/spec_nano2_optics/mirrz/Position, nano1:20000/ID01/spec_nano2_optics/mirty/Position, nano1:20000/ID01/spec_nano2_optics/mirty1/Position, nano1:20000/ID01/spec_nano2_optics/mirty2/Position, nano1:20000/ID01/spec_nano2_optics/mirtz/Position, nano1:20000/ID01/spec_nano2_optics/monotz/Position, nano1:20000/ID01/spec_nano2_optics/monoty/Position, nano1:20000/ID01/spec_nano2_optics/monorz/Position, nano1:20000/ID01/spec_nano2_optics/cc2pit/Position, nano1:20000/ID01/spec_nano2_optics/dcm2pf/Position, nano1:20000/ID01/spec_nano2_optics/dcm2pit/Position, nano1:20000/ID01/spec_nano2_optics/dcm2rlf/Position, nano1:20000/ID01/spec_nano2_optics/dcm2rol/DialPosition, nano1:20000/ID01/spec_nano2_optics/dcm2ty/Position,nano1:20000/ID01/spec_nano2_optics/mir1rz/Position, nano1:20000/ID01/spec_nano2_optics/mir1rzf/Position, nano1:20000/ID01/spec_nano2_optics/mm1rz/Position, nano1:20000/ID01/spec_nano2_optics/mm2tx/Position, nano1:20000/ID01/spec_nano2_optics/mm2ty/Position, nano1:20000/ID01/spec_nano2_optics/mmboff/Position, nano1:20000/ID01/spec_nano2_optics/mmrz/Position, nano1:20000/ID01/spec_nano2_optics/mmty/Position, nano1:20000/ID01/spec_nano2_optics/mmty2/Position, nano1:20000/ID01/spec_nano2_optics/mmtz/Position, nano1:20000/ID01/spec_nano2_optics/tf1rz/Position, nano1:20000/ID01/spec_nano2_optics/tf1y/Position, nano1:20000/ID01/spec_nano2_optics/tf1z1/Position, nano1:20000/ID01/spec_nano2_optics/tf1z2/Position
InstrumentAttenuator_value = nano1:20000/ID01/spec_nano2_blserver/att11/Position, nano1:20000/ID01/spec_nano2_blserver/att12/Position, nano1:20000/ID01/spec_nano2_blserver/att13/Position,nano1:20000/ID01/spec_nano2_blserver/att21/Position
InstrumentDetector01_motors_value= nano1:20000/ID01/spec_nano2_blserver/att11/Position, nano1:20000/ID01/spec_nano2_blserver/att12/Position, nano1:20000/ID01/spec_nano2_blserver/att13/Position,nano1:20000/ID01/spec_nano2_blserver/att21/Position
InstrumentDetector02_motors_value= nano1:20000/ID01/spec_nano2_blserver/att11/Position, nano1:20000/ID01/spec_nano2_blserver/att12/Position, nano1:20000/ID01/spec_nano2_blserver/att13/Position,nano1:20000/ID01/spec_nano2_blserver/att21/Position
```

### Labels

```
InstrumentInsertionDevice_gap_name = U35a U27b U35b U27c
InstrumentInsertionDevice_taper_name = U35a U27c
InstrumentOpticsMotors_name =  monobof mononrj monoth mir1rz mir1rzf mir2crv mir2ty mirboff mirrz mirty mirty1 mirty2 mirtz monotz monoty monorz cc2pit ccboff ccnrj ccthet dcm2pf dcm2pit dcm2rlf dcm2rol dcm2ty dcmboff dcmnrj dcmthet mir1rz mir1rzf mm1rz mm2tx mm2ty mmboff mmrz mmty mmty2 mmtz tf1rz tf1y tf1z1 tf1z2
InstrumentAttenuator_name = att11 att12 att13 att21
InstrumentDetector01_motors_name = att11 att12 att13 att21
InstrumentDetector02_motors_name = att11 att12 att13 att21
InstrumentDetector01_name = detector01
InstrumentDetector02_name = detector02
InstrumentSlits_name = p1 p2 s1 s2 s3 s4 s5 s6
InstrumentSlitPrimary_name = p1
InstrumentSlitSecondary_name = s1
```