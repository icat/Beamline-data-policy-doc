#!/usr/bin/env python

"""A simple client for MetadataManager and MetaExperiment
"""

import os
import sys
import logging
import PyTango.client

class MetadataManagerClient(object):

    metadataManager = None
    metaExperiment = None

    """
    A client for the MetadataManager and MetaExperiment tango Devices

    Attributes:
        name: name of the tango device. Example: 'id21/metadata/ingest'
    """
    def __init__(self, metadataManagerName, metaExperimentName):
        """
    	Return a MetadataManagerClient object whose metadataManagerName is *metadataManagerName*
    	and metaExperimentName is *metaExperimentName*
    	"""
        self.dataRoot = None
        self.proposal = None
        self.sample = None
        self.datasetName = None
        
    	if metadataManagerName:
            self.metadataManagerName = metadataManagerName
        if metaExperimentName:
    		self.metaExperimentName = metaExperimentName

        print('MetadataManager: %s' % metadataManagerName)
        print('MetaExperiment: %s' % metaExperimentName)

       	""" Tango Devices instances """	 
        try:
            MetadataManagerClient.metadataManager = PyTango.client.Device(self.metadataManagerName)
            MetadataManagerClient.metaExperiment = PyTango.client.Device(self.metaExperimentName)
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def printStatus(self):
        print('DataRoot: %s'%MetadataManagerClient.metaExperiment.dataRoot)
        print('Proposal: %s'%MetadataManagerClient.metaExperiment.proposal)
        print('Sample: %s'%MetadataManagerClient.metaExperiment.sample)
        print('Dataset: %s'%MetadataManagerClient.metadataManager.scanName)



    def __setDataRoot(self, dataRoot):
        try:
            MetadataManagerClient.metaExperiment.dataRoot = dataRoot
            self.dataRoot = dataRoot
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    ''' Set proposal should be done before stting the data root '''
    def __setProposal(self, proposal):
        try:
            MetadataManagerClient.metaExperiment.proposal = proposal
            self.proposal = proposal
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    ''' Set proposal should be done before stting the data root '''
    def appendFile(self, filePath):
        try:
            MetadataManagerClient.metadataManager.lastDataFile = filePath          
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise


    def __setSample(self, sample):
        try:
            MetadataManagerClient.metaExperiment.sample = sample
            self.sample = sample
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def __setDataset(self, datasetName):
        try:
            MetadataManagerClient.metadataManager.scanName = datasetName
            self.datasetName = datasetName
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise

    def start(self, dataRoot, proposal, sampleName, datasetName):
        """ Starts a new dataset """
        if MetadataManagerClient.metaExperiment:
            try:
                """ setting proposal """
                self.__setProposal(proposal)

                """ setting dataRoot """
                self.__setDataRoot(dataRoot)               

                """ setting sample """
                self.__setSample(sampleName)

                """ setting dataset """
                self.__setDataset(datasetName)

                """ setting datasetName """
                if (str(MetadataManagerClient.metaExperiment.state()) == 'ON'):
                    if (str(MetadataManagerClient.metadataManager.state()) == 'ON'):
                        MetadataManagerClient.metadataManager.StartScan()
                        
            except:
                print "Unexpected error:", sys.exc_info()[0]
                raise

    def end(self):
        try:
            MetadataManagerClient.metadataManager.endScan()
        except:
            print "Unexpected error:", sys.exc_info()[0]
            raise        

if __name__ == '__main__':
    metadataManagerName = 'id01/metadata/ing_test'
    metaExperimentName  = 'id01/metadata/exp_test'
    client = MetadataManagerClient(metadataManagerName, metaExperimentName)
    
    
    # Elogbook notification
    client.metadataManager.notifyInfo("Dataset datasetName_1 is about to start")
    
    # Dataset starts
    client.start('/data/visitor/id01/MA1313', 'MA1313', 'sampleName', 'datasetName_1')
    
    # Appending files to dataset
    client.appendFile('/data/visitor/id01/MA1313/datasetName_1_1.edf')
    client.appendFile('/data/visitor/id01/MA1313/datasetName_1_2.edf')
    
    # Elogbook notification
    client.metadataManager.notifyDebug("Two files has been added to the dataset")
    
    # Adding metadata
    client.metadataManager.scanType = "MX"
    client.metadataManager.InstrumentSource_energy = "12.834 keV"
    client.metadataManager.MX_transmission = "100%"
    client.metadataManager.MX_flux = "1.5e+11 ph/sec"
    client.metadataManager.MX_fluxEnd = "1.49e+11 ph/sec"
    client.metadataManager.MX__resolution = "3.75 Å"


    client.printStatus()
    # Dataset ends
    client.end()