#! /usr/bin/python
#
# Search the ICAT for all entity types and report the number of
# objects found for each type.
#

from __future__ import print_function
import icat
import icat.config
import logging
import sys
import csv
from distutils.util import strtobool

logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.CRITICAL)


def user_yes_no_query(question):
    sys.stdout.write('%s [y/n]\n' % question)
    while True:
        try:
            return strtobool(raw_input().lower())
        except ValueError:
            sys.stdout.write('Please respond with \'y\' or \'n\'.\n')	


def update( parameterName, newName, newUnits, description ):
    conf = icat.config.Config().getconfig()
    client = icat.Client(conf.url, **conf.client_kwargs)
    client.login(conf.auth, conf.credentials)
    try:
        parameterTypeList = client.search("SELECT (p) FROM ParameterType p where p.name = '%s'" % parameterName )

        if ((parameterTypeList is not None) & (len(parameterTypeList) == 1)):
            parameterType = parameterTypeList[0]
            facility = client.search("SELECT f FROM Facility f" )[0]
            # Add facilility
            if facility is not None:
                parameterType.facility = facility             
	        if newName != None:        
                    parameterType.name = newName        
                if (parameterType.units == None):
                     parameterType.units = "NA"
                if (newUnits != None):
                     parameterType.units = newUnits
                if (description != None):
                     parameterType.description = description   
                 
                client.update(parameterType)
                print("[INFO] Parameter %s has been renamed to %s" % (parameterName, newName))
        else:
            print("[ERROR] ParameterType %s not found" % parameterName)

    except icat.exception.ICATPrivilegesError:      
        parameterType = 0

def emptyToNone(value):   
    if (len(value) == 0):
        return None
    return value

with open('parameters.csv', 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in spamreader:
        
        if len(row) == 4:
            parameterName = row[0].strip()	
            newName = row[1].strip()
            newUnits = emptyToNone(row[2].strip())
            description = emptyToNone(row[3].strip())
            print ("\n\n------------------------------")
            print ("[INFO] Updating parameterType %s" %parameterName)
            print ("[INFO] New name parameterType %s" %newName)

            if newUnits is not None:
                print ("[INFO] New units %s" %newUnits)
            if description is not None:
                print ("[INFO] New description %s" %description)
            if (user_yes_no_query("\n\nAre these values OK?") == False):
                print ("\n\n[INFO] Skipping parameterType %s" %parameterName)
            else:                
                update( parameterName, newName, newUnits, description )



parameterName = 'new' 	
newName = 'physicalLocation'
newUnits = 'mm'
print ("[INFO] Updating %s" %parameterName)



print()
