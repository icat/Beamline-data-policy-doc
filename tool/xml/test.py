import xml.etree.ElementTree as ET
import re

tree = ET.parse('../../hdf5_cfg.xml') 
text = (ET.tostring(tree.getroot(), encoding='utf8', method='text'))

noBlankLines = filter(lambda x: not re.match(r'^\s*$', x), text)

#attributeList = noBlankLines.replace("$", "\n").replace("{", "").replace("}", "")
#print attributeList.split("\n")[1:]

attributeList = noBlankLines.replace("$", "\n").replace("{", "").replace("}", "")
print attributeList

